const assert = require('chai').assert;

function evaluation(arr) {
  switch(arr.score) {
    case 1:
    case 2:
    case 3:
    case 4:
      return {...arr, status: 'eazy'}
    case 5:
    case 6:
    case 7:
      return {...arr, status: 'medium'}
    case 8:
    case 9:
      return {...arr, status: 'hard'}
    default:
      return arr;
  }
}

describe('test evaluation student', function() {
  const ListData = [
    {id: 1, name: 'Kitty01', score: 1},
    {id: 2, name: 'Kitty02', score: 2},
    {id: 3, name: 'Kitty03', score: 3},
    {id: 4, name: 'Kitty04', score: 4},
    {id: 5, name: 'Kitty05', score: 5},
    {id: 6, name: 'Kitty06', score: 6},
    {id: 7, name: 'Kitty07', score: 7},
    {id: 8, name: 'Kitty08', score: 8},
    {id: 9, name: 'Kitty09', score: 9}
  ];

  const ListStudentGood = [
    {id: 1, name: 'Kitty01', score: 9},
    {id: 2, name: 'Kitty02', score: 9},
    {id: 3, name: 'Kitty03', score: 9},
    {id: 4, name: 'Kitty04', score: 9},
  ];

  const ListStudentIdiot = [
    {id: 1, name: 'Kitty01', score: 1},
    {id: 2, name: 'Kitty02', score: 2},
    {id: 3, name: 'Kitty03', score: 3},
    {id: 4, name: 'Kitty04', score: 4},
  ];

  describe('evaluation()', function() {
    describe('evaluation() with student', function() {
      ListData.forEach(function(value) {
        it('correctly evaluation with student', function() {
          const {status, score} = evaluation(value);
          {
            switch(score) {
              case 1:
              case 2:
              case 3:
              case 4:
                return assert.equal(status, 'eazy');
              case 5:
              case 6:
              case 7:
                return assert.equal(status, 'medium');
              case 8:
              case 9:
                return assert.equal(status, 'hard');
              default:
                return assert.equal(status, 'hard');
            }
          }
        });
      });
    });
    describe('evaluation() with student idiot', function() {
      ListStudentGood.forEach(function(value) {
        it('correctly evaluation with student idiot', function() {
          const {status} = evaluation(value);
          assert.equal(status, 'hard');
        });
      });
    });
    describe('evaluation() with student good', function() {
      ListStudentIdiot.forEach(function(value) {
        it('correctly evaluation with student good', function() {
          const {status} = evaluation(value);
          assert.equal(status, 'eazy');
        });
      });
    });
  });
});
