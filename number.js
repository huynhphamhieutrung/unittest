const assert = require('chai').assert;

function numberOdd(arr) {
  let sum = 0;

  for (const i of arr) {
    if (0 === i % 2) {
      sum += 1;
    }
  }
  return sum;
}

describe('Ham test function dem so chan', function() {
  const tests = [
    {args: [1, 2, 3, 4, 5, 6, 7, 8, 9], expected: 4},
    {args: [1, 3, 5, 7, 9], expected: 0},
    {args: [2, 4, 6], expected: 3},
  ];

  describe('numberOll()', function() {
    tests.forEach(function(test) {
      it('numberOll() odd and even with ' + test.args.length + ' value', function() {
        const res = numberOdd(test.args);
        assert.equal(res, test.expected);
      });
    });
    it('numberOdd() arr []', function() {
      assert.equal(numberOdd([]), 0);
    });
    it('numberOdd() with only odd', function() {
      assert.equal(numberOdd([2,4]), 2);
    });
    it('numberOdd() with only even', function() {
      assert.equal(numberOdd([1,3]), 0);
    });
  });
});
